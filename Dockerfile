FROM ubuntu

RUN apt-get -y update
RUN apt-get install -y nodejs
RUN apt-get -y install npm
RUN npm install -g gulp
RUN ln -s /usr/bin/nodejs /usr/bin/node;

ADD . /eat
WORKDIR /eat

RUN cd /eat && npm install

EXPOSE  8888

CMD ["gulp"]
